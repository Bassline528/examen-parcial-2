<?php
// Implementar un script PHP que haga lo siguiente:
// • Crear una función que genere códigos de 4 letras (A-Z) aleatorias.
// • Crear un array de 500 elementos cuyo índice sea numérico y cuyo valor sean
// los códigos de 4 letras generados con la función anterior.
// • Imprimir ese array utilizando la estructura foreach. Se debe imprimir de
// manera tabular indicando índice y valor del array generado aleatoriamente.

function getRandomString($length = 4) {
    $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $string = '';

    for ($i = 0; $i < $length; $i++) {
        $string .= $characters[mt_rand(0, strlen($characters) - 1)];
    }

    return $string;
}


$examsArray = array();

$i = 1;

while ($i <= 500) {
    $examsArray[$i] = getRandomString();
    $i++;
}
echo "<table>";
    echo "<tr>";
        echo "<td style='border: 1px solid black; background-color:red;'>Indice</td>";
        echo "<td style='border: 1px solid black; background-color:red;'>Valor</td>";
    echo "</tr>"; 
    
    
foreach ($examsArray as $key => $value) {
    echo "<tr>";
    echo "<td style='border: 1px solid black'>$key</td>";
    echo "<td style='border: 1px solid black'>$value</td>";
    echo "</tr>"; 
}

echo "</table>";



